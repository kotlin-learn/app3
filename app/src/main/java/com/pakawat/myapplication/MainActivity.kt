package com.pakawat.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.pakawat.myapplication.adapter.ItemAdapter
import com.pakawat.myapplication.data.DataSouce
import com.pakawat.myapplication.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private var _binding: ActivityMainBinding? = null
    // This property is only valid between onCreateView and
// onDestroyView.
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        // loadProduct
        val data = DataSouce().loadProducts()

        //recyclerView
        val recyclerView = binding.recyclerView
        recyclerView.adapter = ItemAdapter(this.applicationContext,data)
        recyclerView.layoutManager = LinearLayoutManager(this)
    }
}