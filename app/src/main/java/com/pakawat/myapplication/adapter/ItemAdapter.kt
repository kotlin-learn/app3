package com.pakawat.myapplication.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.pakawat.myapplication.R
import com.pakawat.myapplication.adapter.ItemAdapter.ViewHolder
import com.pakawat.myapplication.model.Oil

class ItemAdapter(private val context: Context, private val data: List<Oil>) : RecyclerView.Adapter<ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val product = itemView.findViewById<TextView>(R.id.textview_product)
        val price = itemView.findViewById<TextView>(R.id.textview_price)
        val item=   itemView.findViewById<LinearLayout>(R.id.linearLayout)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val adapterLayout =
            LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
        return ViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.product.text = data[position].name
        holder.price.text = data[position].price.toString()
        holder.item.setOnClickListener {
            Log.d("MAINACTIVITY","${data[position].name} ${data[position].price.toString()}")
            Toast.makeText(this.context,"${data[position].name} ${data[position].price.toString()}",Toast.LENGTH_LONG).show()
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

}
