package com.pakawat.myapplication.data

import com.pakawat.myapplication.model.Oil

class DataSouce {
    fun loadProducts(): List<Oil>{
        return listOf<Oil>(
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ E2",37.24),
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 91",38.08),
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 95",38.35),
            Oil("เชลล์ วี-เพาเวอร์ แก๊สโซฮอล์ 95",45.84),
            Oil("เชลล์ ดีเซล B2",36.34),
            Oil("เชลล์ ฟิวเซฟ ดีเซล",36.34),
            Oil("เชลล์ ฟิวเซฟ ดีเซล B7",36.34),
            Oil("เชลล์ วี-เพาเวอร์ ดีเซล",36.34),
            Oil("เชลล์ วี-เพาเวอร์ ดีเซล B7",47.06),
            )
    }
}